---
title: "Guide de survie des doctorants du LabPsy"
format: 
  html: 
    theme: [dark, styleGuideDoc.css]
    toc: true
---

*Que vous soyez doctorant au LabPsy depuis quelques jours ou plusieurs années, ce document compile différentes informations et ressources qui vous seront utiles lors de votre parcours de thèse.*

## Qui aller voir et à quel moment ? 

### Les personnes ressources du LabPsy : 

#### - Elisabeth & Marie : Ce sont les gestionnaires administratives et financières du Laboratoire. 

Elles pourront répondre à vos questions concernant vos déplacements, inscriptions colloque, demande de matériel, impressions, etc…

*-> Plus d’informations sur le site du LabPsy* : [Elisabeth de Salengre](https://labpsy.u-bordeaux.fr/membres/accompagnement-de-la-recherche/elisabeth-de-salengre) & [Marie Duhamel](https://labpsy.u-bordeaux.fr/membres/accompagnement-de-la-recherche/duhamel-marie) 

#### - Solenne : C’est l’ingénieure en production, traitement et analyse de données du Laboratoire. 

Donc en gros dès que vous avez une question qui concerne vos données de recherche, c’est elle qu’il faut contacter. Que cela concerne la production de données (Questionnaires en ligne, etc), le traitement de données (Questions statistiques ou script R par exemple), le RGPD (je dois vraiment faire une déclaration ? ou pas ?), le stockage des données et le partage de fichiers (Milkyway, RESANA ou autre) ou encore la Science Ouverte (c’est quoi ce truc ?).

*-> Plus d’informations sur le site du LabPsy* : [Solenne Roux](https://labpsy.u-bordeaux.fr/membres/accompagnement-de-la-recherche/solenne-roux)

#### - Les représentants des doctorants

Il y en a dans différentes instances de l’Université : 

- Au niveau du **LabPsy**, un représentant titulaire et un suppléant sont élus pour une durée de 2 ans afin de vous représenter au niveau du Conseil de Laboratoire. 
- Au niveau de **l’Ecole Doctorale**, un représentant titulaire et un suppléant sont élus afin de vous représenter au niveau du Conseil de l’Ecole Doctorale SP2.
- Au niveau du **département Scientifique ECOr**, un représentant titulaire et un suppléant sont élus afin de vous représenter au niveau du Conseil d’ECOr.

#### - Votre direction de thèse

Avant toute démarche au sein du LabPsy **(Par exemple : demande de financement, de matériel, etc.)** vous devez systématiquement avoir l'accord de votre direction de thèse. Sans cet accord préalable, vos demandes ne seront pas examinées.

## Quelles démarches dois-je entreprendre ? Quelles ressources dois-je utiliser ? 

### Demandes de financement 

- Si vous êtes doctorants :Pour toute demande de financement vous devez d’abord obtenir l’accord de votre direction de thèse puis de votre responsable de programme. Si cela est validé à ces deux niveaux, votre financement sera pris en charge par votre programme de rattachement. 

- Pour les ATER, comme pour les soutenances de thèses, la demande doit être réalisée auprès de la direction du laboratoire et votre demande sera, selon les cas, traitées directement par la direction (si elle correspond aux critères en vigueur) ou examinée en Conseil de Laboratoire.

### Stagiaires

Les doctorants ne peuvent pas encadrer de stagiaires en leur nom propre. Seul les membres titulaires du laboratoire peuvent être maître de stage.

### Règlement intérieur du Laboratoire

Un règlement intérieur a été voté en Assemblée Générale du Laboratoire afin d'encadrer les droits et devoirs des différents membres du LabPsy.

[Règlement intérieur du LabPsy](annexes/RIUR4139.pdf)

### Produire / Récolter mes données

####   Protection des données personnelles / Déontologie / Ethique

Avant de lancer ma collecte de données je dois m’assurer que je ne collecte pas de données personnelles. C’est quoi ça ? : 

o	Des données directement identifiantes *(nom, prénom, adresse mail, numéro de téléphone, image d’une personne, vidéo, voix, etc.)*
o	Des données indirectement identifiantes *(Croisement de plusieurs informations : p. ex. Profession et lieu de résidence, profession et entreprise, etc.)*

-> **Je n’ai pas de données personnelles** : pas de soucis, je peux collecter mes données sans rien de plus (pour être dans les clous du [RGPD](https://www.cnil.fr/fr/reglement-europeen-protection-donnees), règlement général sur la protection des données).

-> **Je m’apprête à collecter des données personnelles** : Mise en conformité avec le Règlement Général sur la Protection des Données. Je prends RDV avec mon directeur / ma directrice de thèse afin de remplir conjointement le document : « RENSEIGNEMENTS RELATIFS A LA CONFORMITE AU RGPD D’UN TRAITEMENT DE DONNEES A CARACTERE PERSONNEL DANS LE CADRE D’UN PROJET DE RECHERCHE ». Le porteur de la recherche sera nécessairement votre directeur ou votre directrice de thèse, même si les données collectées le sont dans le cadre de votre thèse. 
Ce document sert à enregistrer votre traitement de données auprès du DPO (Délégué à la protection des données - *Data Protection Officer*) de votre structure, l'Université de Bordeaux. Votre direction de thèse devra donc le renvoyer au DPO de l'Université de Bordeaux à l'adresse : dpo@u-bordeaux.fr.
Le DPO doit vous informer que votre collecte de données est conforme au RGPD avant que vous la commenciez. Vous devez donc remplir et retourner ce document bien avant votre phase de collecte des données. Comptez au minimum 3 mois de délai. 

[Documents utiles RGPD](annexes/DocDPOUB.docx)

•	Attention : Que vous récoltiez ou non des données personnelles, vous aurez malgré tout besoin de faire remplir un consentement éclairé aux participants de votre étude, afin d’être en conformité avec le code de déontologie des psychologues. Un document a été créé à cet effet pour le LabPsy et s’intitule : « DECLARATION DE CONSENTEMENT LIBRE ET ÉCLAIRÉ POUR LA PARTICIPATION A UN PROJET DE RECHERCHE »

[Consentement au LabPsy](annexes/consentementLabPsy2023.docx)

•	Selon la recherche menée, elle peut nécessiter un avis du comité d'éthique. L'Université de Bordeaux a un comité d'éthique de la Recherche ([CER](https://www.u-bordeaux.fr/recherche/ethique-de-la-recherche/comite-ethique-pour-la-recherche)), auprès duquel votre direction de thèse peut déposer une demande concernant votre travail de thèse. 


#### Logiciels de recueil de données / Bases de données secondaires

•	Au LabPsy nous disposons d’une plateforme de questionnaires en ligne sous Limesurvey et hébergée sur les serveurs de l’UB : [https://psysurvey.u-bordeaux.fr/limesurvey/index.php/admin/authentication/sa/login](https://psysurvey.u-bordeaux.fr/limesurvey/index.php/admin/authentication/sa/login)
Elle est gratuite et répond aux exigences en matière de protection des données personnelles, puisque les données sont hébergées en France, sur les serveurs de l’Université de Bordeaux.
-> Afin d’obtenir un compte sur cette plateforme, il vous suffit de faire la demande à Solenne Roux. 
ATTENTION cette plateforme n’est accessible qu’aux membres du LabPsy (donc pas aux étudiants de licence, ni de master).

•	Une autre solution de questionnaires en ligne : la plateforme psytoolkit : [https://www.psytoolkit.org/](https://www.psytoolkit.org/)
Elle est gratuite et répond aux exigences en matière de protection des données personnelles, car en choisissant le serveur européen (celui que vous devez choisir), vos données seront hébergées en France (à Strasbourg). 
Cette plateforme est ouverte à tous, sous réserve de se créer un compte. Elle permet de faire des questionnaires en ligne et des expériences programmées par ordinateur. 

:::{.callout-warning icon=false title="Ne pas utiliser n'importe quoi"}
**A ne surtout pas faire** : 
Ne pas utiliser les outils en ligne qui ne vous apportent aucune garantie sur le stockage et la diffusion de vos données tels que Googleform, Framaforms, etc. 
:::

•	Vous pouvez aussi réutiliser des données, collectées précédemment par d’autres équipes de recherche ou services statistiques qui font ça dans les règles de l'art.

Plusieurs plateformes existent, en voici quelques-unes :

o	Bases de données des grandes enquêtes en France (en général réalisées par l’INSEE,  l’INED ou des services statistiques ministériels, mais pas seulement) : [https://www.data.gouv.fr/fr/](https://www.data.gouv.fr/fr/)
o	Bases de données issues de la recherche en France : [https://recherche.data.gouv.fr/fr](https://recherche.data.gouv.fr/fr)
o	Répertoire international d’entrepôts de données : [https://www.re3data.org/](https://www.re3data.org/)
o	L’entrepôt Open Science Framework (OSF), très utilisé en Psychologie, comprend également de nombreuses bases de données : [https://osf.io/](https://osf.io/)


#### Expériences programmées par ordinateur

•	Concernant les expériences programmées par ordinateur vous pouvez donc utiliser Psytoolkit ou tout autre solution gratuite. [PsychoPy](https://www.psychopy.org/) est installé sur les ordinateurs portables et les ordinateurs des boxes.
Le laboratoire dispose de 2 clefs Eprime (merci de me les rapporter après utilisation pour que les autres puissent aussi en bénéficier). La version d’Eprime est Eprime 2. Il n’est pas prévu d’acquérir de licence Eprime 3.

•	Le laboratoire dispose de matériel de recherche tel que :

- 3 Modules Biopac permettant de faire des mesures de réponse électro-dermale (2 modules), un module électromyogramme, ainsi qu’un module électrocardiogramme. 
- Du matériel d’eye tracking (tobii) 
- Du matériel de reconnaissance des émotions faciales (FaceReader).

 Avant de réserver ce type de matériel, délimitez bien le temps nécessaire dont vous aurez besoin (en concertation avec votre directeur de thèse), car plusieurs personnes peuvent en avoir besoin sur la même période. 

•	Vous le savez probablement déjà mais le LabPsy dispose aussi de salles d’expérimentation : 

o	7 boxes expérimentaux
o	1 salle d’entretien
o	1 salle d’observation avec miroir sans tain et matériel d’enregistrement audio intégré

:::{.callout-tip icon=false title="Réservation du matériel"}
Les réservations du matériel de recherche et des salles d'expérimentation du LabPsy s'effectuent sur le site de gestion et réservation de ressources du LabPsy ([GRR](https://labopsycho.u-bordeaux.fr/GRR/login.php)). Vous ne pouvez pas avoir de compte en tant que doctorant, vous devez vous rapporcher de votre direction de thèse pour toute demande de réservation.
:::

### Bibliographie

•	Un outil gratuit vous permettant de constituer vos bibliographies et de les organiser : [Zotero](https://www.zotero.org/)
Vous pouvez travailler en local sur votre ordinateur ou en ligne et partager des bibliographies avec d’autres personnes. Vous pouvez exporter vos bibliographies sous différents formats dont un aux normes APA.


### Travailler en équipe / Partager ses fichiers

Possibilité d’avoir des dossiers individuels et collectifs (par projet) :

•	Le serveur interne du LabPsy : Milkyway (si vous n’y avez pas encore accès, demander les codes à Solenne Roux). 
•	Service de l’UB : [UBCloud](https://ubcloud.u-bordeaux.fr/login)

Espaces collaboratifs par projet (pas d’espace individuels) : 

•	Service d’Huma-Num (CNRS) : [sharedocs](https://documentation.huma-num.fr/sharedocs-stockage/) 
•	Ou encore la plateforme développée pour l'ensembles des agents de la fonction publique pendant le confinement : [RESANA](https://resana.numerique.gouv.fr/public/utilisateur)

Le [gitlab](https://gitub.u-bordeaux.fr/users/sign_in) de l’UB permet de déposer différents documents et d’avoir un suivi des contrôles de versions. Pratique pour mettre ses scripts et les partager.
Le [gitlab d'Huma-Num](https://gitlab.huma-num.fr/users/sign_in) permet en plus de celui de l'UB de développer des pages et donc de diffuser des présentations en ligne.

:::{.callout-warning icon=false title="Point d'alerte"}
Tout comme les questionnaires en ligne, vous devez faire attention à l’endroit où vous stockez vos données. Les outils tels que DropBox, Iclouddrive ou GoogleDrive ne vous garantissent rien en matière de protection et de sauvegarde des données.
:::

### Traitement de données

#### Pré-enregistrement

C’est quoi ce truc ? Le pré-enregistrement a été proposé et s’est développé suite à la crise de la reproductibilité qui a secoué la Psychologie ces dernières décennies. 

Le pré-enregistrement c’est quoi : [https://www.apa.org/pubs/journals/resources/preregistration](https://www.apa.org/pubs/journals/resources/preregistration)
Pour débuter dans le pré-enregistrement, des outils utiles : [AsPredicted](https://aspredicted.org/)
Mise à disposition de son pré-enregistrement : Sur tout entrepôt de données, en Psychologie la plus utilisée reste [OSF](https://osf.io/)

#### Traitement statistiques

Les logiciels disponibles au laboratoire : 

Uniquement les solutions gratuites sont disponibles pour l’ensemble des membres du laboratoire. Certaines équipes ont fait le choix d’acquérir Mplus, mais ce n’est pas le cas de l’ensemble du laboratoire et le logiciel n’est pas installé sur des machines accessibles à tous. 

Vous pouvez donc utiliser : R / Rstudio ou Python.

N’hésitez pas à prendre RDV avec Solenne si vous avez besoin de conseils, ou de fonction R pour vos traitements statistiques. 

### Stocker ses données et les mettre à disposition

#### Entrepôts de données

Vous pouvez déposer vos données sur différents entrepôts de données lorsque vos recherches sont terminées (ou que votre article est publié). 
*Vous pouvez déposer vos bases de données issues de questionnaires, d’expérimentations ou d’entretiens qualitatifs. Il faut bien veiller à anonymiser vos données avant de les déposer sur un entrepôt de données.*

En Psychologie l’entrepôt le plus utilisé est [OSF](https://osf.io/) (Bien veiller à choisir des serveurs en Europe).

Sinon il existe d’autres entrepôts : 

- [Zenodo](https://zenodo.org/), entrepôt initialement dédié au CERN, actuellement à disposition de tous. 
- [Nakala](https://www.nakala.fr/), entrepôt de données français développé dans le cadre des outils Huma-Num.

#### Publier en Open Access sans se ruiner c’est vraiment possible ?

Oui !! Ne vous ruinez pas en payant des APC très chères à des revues pour pouvoir publier vos articles en Open Access.

 Vous devez déposer vos post-print auteur sur des plateformes de dépôt de publication scientifique (archive ouverte) après un embargo d’1 an (les plateformes lèvent les embargos elles-mêmes à échéance de la date). Tout ça grâce à l’article 6 de la loi pour une république numérique de 2016.
 
Pour toute question sur le sujet vous pouvez contacter directement le service de la Direction de la Documentation de l’Université de Bordeaux dédié au soutien à la recherche : [https://bibliotheques.u-bordeaux.fr/Soutien-a-la-recherche](https://bibliotheques.u-bordeaux.fr/Soutien-a-la-recherche)

Où déposer son post-print auteur ? : 

- [HAL](https://hal.science/) : l’archive ouverte du CCSD (Centre pour la Communication Scientifique Directe)
- [OSKAR Bordeaux](https://oskar-bordeaux.fr/) : L’archive ouverte institutionnelle de l’Université de Bordeaux. 

*Il est à noter que ces deux archives ouvertes sont interconnectées.*

### Ressources utiles :

- Les ressources d’Ouvrir la Science : [https://www.ouvrirlascience.fr/category/ressources/](https://www.ouvrirlascience.fr/category/ressources/)
- Les ressources recensées dans le cadre des [journées SOFT](https://soft.sciencesconf.org/resource/page/id/10)


## Vie quotidienne

#### On travaille ensemble

Au-delà des outils de travail collaboratif, les règles du LabPsy sont détaillées dans le règlement intérieur du Laboratoire.

#### On vit ensemble

Oui on vit ensemble et on doit faire attention aux biens communs et à ses affaires. 
Cela implique : le respect d’autrui, le fait de garder un environnement propre (bureaux, cuisine) et désencombré (merci de ne pas laisser s’accumuler les affaires personnelles), de remettre en place le matériel utilisé (pour que d’autres puissent s’en servir). 
 
**Partage d'expérience**

![](images/Guide_1_ciel_mon_ordi_1.png)

:::{.callout-note icon=false appearance="simple"}
Cette scène s’est déroulée en 2013 au LabPsy. C’était il y a 11 ans, certes, mais des vols ont lieu régulièrement dans l’enceinte du Laboratoire. Merci de fermer la porte de votre bureau lorsque vous vous absentez, même pour 2 minutes…
:::
